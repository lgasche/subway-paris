Doctests pour chaque fonction.

>>> from graphe import *
>>> from ameliorations import *

Récupérer les données de la ligne de métro 3b:
>>> reseau = Graphe()
>>> charger_donnees(reseau, "METRO_3b.txt")

Afficher les identifiants des sommets:
>>> sorted(reseau.sommets())
[1659, 1718, 1752, 1783]

Afficher les noms des sommets:

Afficher les arêtes:
>>> sorted(reseau.aretes())
[(1659, 1783, 'METRO_3b'), (1718, 1752, 'METRO_3b'), (1718, 1783, 'METRO_3b')]

Rajouter au réseau les données de la ligne de métro 14:
>>> charger_donnees(reseau, "METRO_14.txt")

Afficher les identifiants des sommets:
>>> sorted(reseau.sommets())
[1659, 1718, 1722, 1752, 1757, 1783, 1869, 1955, 1964, 2068, 1166824, 1166826, 1166828]

Afficher les arêtes:
>>> sorted(reseau.aretes())
[(1659, 1783, 'METRO_3b'), (1718, 1752, 'METRO_3b'), (1718, 1783, 'METRO_3b'), (1722, 1869, 'METRO_14'), (1757, 1869, 'METRO_14'), (1757, 1964, 'METRO_14'), (1955, 1964, 'METRO_14'), (1955, 2068, 'METRO_14'), (2068, 1166828, 'METRO_14'), (1166824, 1166826, 'METRO_14'), (1166826, 1166828, 'METRO_14')]

Afficher les points d'articulations :
>>> sorted(points_articulation(reseau))
[1718, 1757, 1783, 1869, 1955, 1964, 2068, 1166826, 1166828]

Afficher les ponts :
>>> sorted(ponts(reseau))
[[1718, 1783], [1752, 1718], [1757, 1869], [1783, 1659], [1869, 1722], [1955, 1964], [1964, 1757], [2068, 1955], [1166824, 1166826], [1166826, 1166828], [1166828, 2068]]
