import math
import argparse
from graphe import *

# 3.a
def charger_donnees(graphe, fichier):
    "Ajoute au graphe les données du fichier dont le nom est passé en paramètre."
    sommets = list()
    aretes = list()
    noms = list()
    graphe.name = fichier[0: fichier.find('.')]
    all_lines = open(fichier, "r").read().splitlines()
    for l in all_lines:
        # Find the part stations.
        if l.find(':') != -1:
            list_words = l.split(':')
            sommets.append(int(list_words[0]))
            # (key: id, value: station_name)
            graphe.stations_name[int(list_words[0])] = list_words[1]
        # Find the part connexions.
        if l.find('/') != -1:
            list_words = l.split('/')
            aretes.append([int(list_words[0]), int(list_words[1]), graphe.name])
    graphe.ajouter_sommets(sommets)
    graphe.ajouter_aretes(aretes)
    graphe.station_name_sorted = sorted(graphe.stations_name.items(), key = lambda kv:(kv[1], kv[0]))

# 3.b
def numerotations(G):
    """Les numérotations début et ancêtre des sommets dans une forêt d’exploration enprofondeur pour G, 
    ainsi que les relations de parenté correspondantes."""
    debut = dict()
    parent = dict()
    ancetre = dict()
    for key in G.sommets():
        debut[key] = 0
        parent[key] = None
        ancetre[key] = math.inf
    instant = 0
    def numerotations_recursive(s):
        nonlocal instant, debut, parent, ancetre
        instant = instant + 1
        ancetre[s] = instant
        debut[s] = ancetre[s]
        for t in G.voisins(s):
            if debut[t[0]] != 0:
                if parent[s] != t[0]:
                    ancetre[s] = min(ancetre[s], debut[t[0]])
            else:
                parent[t[0]] = s
                numerotations_recursive(t[0])
                ancetre[s] = min(ancetre[s], ancetre[t[0]]) 
    for v in sorted(G.sommets()):
        if debut[v] == 0:
            numerotations_recursive(v)
    return debut, parent, ancetre


def points_articulation(reseau):
    """Renvoie les points d’articulation du réseau sous la forme d’un itérable."""
    debut, parent, ancetre = numerotations(reseau)
    articulations = set()
    racines = set()
    # Traitement des racines des arbres d’exploration.
    for v in reseau.sommets():
        if parent[v] == None:
            racines = racines.union({v})
    for depart in racines:
        count = 0
        for i in parent:
            if (parent[i] != None) and (depart == parent[i]):
                count += 1
        if count >= 2:
            articulations = articulations.union({depart})
    # Traitement des autres sommets.
    racines = racines.union({None})
    for v in reseau.sommets():
        if (parent[v] not in racines) and (ancetre[v] >= debut[parent[v]]):
            articulations = articulations.union({parent[v]})
    return articulations

# 3.c
def ponts(reseau):
    """Renvoie les ponts du réseau sous la forme d’un itérable d’arêtes."""
    debut, parent, ancetre = numerotations(reseau)
    ponts = list()
    for v in parent:
        if parent[v] != None:
            u = parent[v]
            if ancetre[v] > debut[u]:
                ponts.append([v, u])
    return ponts

# 3.d
def amelioration_ponts(reseau):
    """Renvoie un ensemble d’arêtes à rajouter au réseau pour qu’il ne contienne plus de ponts."""
    aretes = list()
    debut, parent, ancetre = numerotations(reseau)
    ponts_list = ponts(reseau)
    # Le parent n'est pas v et n'est pas extraimité d'un autre pont.
    for u,v in ponts_list:
        feuille = u
        while (parent[feuille] != v) and (parent[feuille] not in ponts_list) and (parent[feuille] != None):
            feuille = parent[feuille]
        if feuille != u:
            aretes.append((u, feuille))
    # Le parent n'est pas u et n'est pas extraimité d'un autre pont.
    for u,v in ponts_list:
        feuille = v
        while (parent[feuille] != u) and (parent[feuille] not in ponts_list) and (parent[feuille] != None):
            feuille = parent[feuille]
        if feuille != v:
            aretes.append((feuille, u))
    return aretes

# 3.e
def trouver_fils(reseau, s):
    """Renvoie un ensemble de sommets, correspondant aux fils de s."""
    debut, parent, ancetre = numerotations(reseau)
    fils = list()
    for sommet in reseau.sommets():
        if parent[sommet] == s:
            fils.append(sommet)
    return fils

def amelioration_points_articulation(reseau):
    """Renvoie un ensemble d’arêtes à rajouter auréseau pour qu’il ne contienne plus de points d’articulation."""
    aretes = list()
    debut, parent, ancetre = numerotations(reseau)
    sorted_debut = {k: v for k, v in sorted(debut.items(), key=lambda item: item[1])}
    points_list = points_articulation(reseau)

    for s in parent:
        if parent[s] == None:
            racine = s
    
    for p in points_list:
        # Cas racine.
        if p == racine:
            fils = trouver_fils(reseau, p)
            aretes.append((fils[0], fils[1]))

        # Trouver ses fils
        for s in reseau.sommets():
            if s not in points_list:
                if parent[s] == p:
                    aretes.append((s, racine))
    # Cas autre sommet.
    return aretes

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--metro", 
                        help = "--metro [lignes] : précise les lignes de métro que l’on veut charger dans le réseau, avec [lignes] facultative.",
                        nargs = "*"
                        )
    
    parser.add_argument("--rer", 
                        help = "--rer [lignes] : précise les lignes de rer que l’on veut charger dans le réseau, avec [lignes] facultative.",
                        nargs = "*"
                        )

    parser.add_argument("--liste-stations", 
                        help = "-liste-stations: affiche la liste des stations du réseau avec leur identifiant triées par ordre alphabétique.",
                        action = "store_true"
                        )

    parser.add_argument("--articulations", 
                        help = "--articulations: affiche les points d’articulation du réseau qui a été chargé.",
                        action = "store_true"
                        )

    parser.add_argument("--ponts", 
                        help = "--ponts: affiche les ponts du réseau qui a été chargé.",
                        action = "store_true"
                        )

    parser.add_argument("--ameliorer-articulations", 
                        help = "--ameliorer-articulations: affiche les points d’articulation du réseau qui a été chargé,ainsi que les arêtes à rajouter pour que ces stations ne soient plus des points d’articulation.",
                        action = "store_true"
                        )

    parser.add_argument("--ameliorer-ponts", 
                        help = "--ameliorer-ponts: affiche les ponts du réseau qui a été chargé, ainsi que les arêtes à rajouter pour que ces arêtes ne soient plus des ponts.",
                        action = "store_true"
                        )

    args = parser.parse_args()

    if args.metro != None:
        G = Graphe()
        if len(args.metro) == 0:
            print("Chargement de toutes les lignes de metro ... terminé.\n")
            for i in range(1, 15):
                charger_donnees(G, "METRO_" + str(i) + ".txt")
        elif len(args.metro) > 0 :
            print("Chargement des les lignes " + str(args.metro) + " de metro ... terminé.\n")
            for i in args.metro:
                charger_donnees(G, "METRO_" + str(i) + ".txt")
        print("Le réseau contient " + str(len(G.sommets())) + " sommets et " + str(len(G.aretes())) +" arêtes.\n") 
    
    if args.rer != None:
        G = Graphe()
        rer_l = ["A", "B"]
        if len(args.rer) == 0:
            print("Chargement de toutes les lignes de rer ... terminé.")
            for i in rer_l:
                charger_donnees(G, "RER_" + str(i) + ".txt")
        elif len(args.rer) > 0 :
            print("Chargement des les lignes " + str(args.rer) + " de rer ... terminé.")
            for i in args.rer:
                charger_donnees(G, "RER_" +  str(i) + ".txt")  
        print("Le réseau contient " + str(len(G.sommets())) + " sommets et " + str(len(G.aretes())) +" arêtes.\n")       

    if args.liste_stations == True:
        for k, v in G.station_name_sorted:
            print("{} ({})".format(v, k))

    if args.articulations == True:
        l = list()
        points_articulation = points_articulation(G)
        print("Le réseau contient les " + str(len(points_articulation)) +" points d'articulation suivants:")
        # Créer la liste contenant tous les nom des points d'articulation.
        for i in points_articulation:
            l.append(G.stations_name[i])
        count = 1
        for name in sorted(l):
            print("   " + str(count) + " : " + name)
            count = count + 1
        print()
    
    if args.ponts == True:
        l = list()
        ponts_list = ponts(G)
        print("Le réseau contient les " + str(len(ponts_list)) + " ponts suivants:")
        # Créer la liste contenant tous les nom des ponts.
        for i in ponts_list:
            l.append((G.stations_name[i[0]], G.stations_name[i[1]]))
        for src, dst in sorted(l):
            print("   - " + str(src) + " -- " + str(dst))
        print()

    if args.ameliorer_articulations == True:
        l = list()
        ponts_list = amelioration_points_articulation(G)
        print("On peut éliminer tous les points d'articulation du réseau en rajoutant les " + str(len(ponts_list)) + " arêtes suivantes:")
        # Créer la liste contenant tous les nom des arêtes à rajouter.
        for i in ponts_list:
            l.append((G.stations_name[i[0]], G.stations_name[i[1]]))
        for src, dst in sorted(l):
            print("   - " + str(src) + " -- " + str(dst))
        print()

    if args.ameliorer_ponts == True:
        l = list()
        ponts_list = amelioration_ponts(G)
        print("On peut éliminer tous les ponts du réseau en rajoutant les " + str(len(ponts_list)) + " arêtes suivantes:")
        # Créer la liste contenant tous les nom des ponts à rajouter.
        for i in ponts_list:
            l.append((G.stations_name[i[0]], G.stations_name[i[1]]))
        for src, dst in sorted(l):
            print("   - " + str(src) + " -- " + str(dst))
        print()
